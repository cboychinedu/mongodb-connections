const MongoClient = require('mongodb').MongoClient; 
let url = 'mongodb://localhost:27017/'; 

// 
MongoClient.connect(url, function(err, db)
{
    // Checking for errors 
    if (err) throw err; 
    let dbo = db.db("mydb"); 

    // 
    dbo.collection('customers').drop(function(err, delOk)
    {
        // 
        if (err) throw err; 
        if (delOk) {console.log("Collection deleted!")}

        // Closing up 
        db.close(); 
    })
})