/* To insert multiple documents into a collection in MongoDB, we use the "insertMany()" methd. */
//
let MongoClient = require('mongodb').MongoClient;
let url = "mongodb://localhost:27017/";

//
MongoClient.connect(url, function(err, db)
{
    //
    if (err) throw err;

    //
    let dbo = db.db("mydb");

    // Creating multiple documents
    let myobj = [
      {
        News: "Lake Tekapo, South Island, New Zealand, Oceania", UserName: "@dectypto", Message: "#tipestry #pics #photography #nature #discussion #pics ",
        TimePresent: "3 days ago", Likes: "10K", DisLikes: "1.9K", Comments: "2", Shares: "10", MoneyRatings: "7", ActualMoney: "1",
        ImageURL: "https://image.thum.io/get/iphoneX/width/100/auth/3228-www.tipestry.com/https://cointelegraph.com/news/dogecoin-trading-volume-hits-5b-surpassing-bitcoin-s-for-the-first-time-ever"

      },
      {
        News: "Beauty" ,UserName: "@fugill8", Message: "#youtube (youtu.be)", TimePresent: "4 days ago",
        Likes: "0.6K", DisLikes: "1.7K", Comments: "6", Shares: "20", MoneyRatings: "9", ActualMoney: "3",
        ImageURL: "https://tipestry.com/api/topic/get/img/0x0-60145ee3b4064a2b41b495aa.jpg"
      },
      {
        News: "Les Planteurs De Pommes De Terre By Jean-Francois Millet", UserName: "@ladyfaucet15206", Message: "#painting #realism #oil_on_canvas #pics (pinimg.com)",
        Timepresent: "4 days ago",  Likes: "5K", Dislikes: "1.1K", Comments: "4",
        Shares: "28", MoneyRatings: "100", ActualMoney: "234",
        ImageURL: "https://tipestry.com/api/topic/get/img/0x0-601382a2339d9843220cc703.jpeg"
      },
      {
        News: "Amazing", UserName: "@anumtaa", TimePresent: "3 days ago", Likes: "50K", DisLikes: "1.87K", Comments: "5",
        Shares: "20", MoneyRatings: "200", ActualMoney: "0.87",
        ImageURL: "https://tipestry.com/api/topic/get/img/0x0-60144ae0b4064a2b41b47b0d.jpg"
      },
      {
        News: "Super Cute Kitten", UserName: "@zubiya", TimePresent: "3 days ago", Likes: "10K", DisLikes: "1.9K", Comments: "2",
        Shares: "10", MoneyRatings: "18", ActualMoney: "287",
        ImageURL: "https://tipestry.com/api/topic/get/img/150x150-6014c54eb4064a2b41b54033.jpg"
      },
    ];

  //
  dbo.collection("customers").insertMany(myobj, function(err, res)
  {
      if (err) throw err;

      //
      console.log("Number of documents inserted: " + res.insertedCount);
      db.close();
  });
});
