//
let MongoClient = require('mongodb').MongoClient;
let url = "mongodb://localhost:27017/";

MongoClient.connect(url, function(err, db)
{
    //
    if (err) throw err;

    //
    let dbo = db.db("mydb");
    dbo.createCollection("customers", function(err, res)
  {
      // Checking if there was an error creating the collection
      if (err) throw err;

      //
      console.log("Collection created!");
      db.close();
  });
}); 
