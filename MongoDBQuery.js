// Importing the mongodb client
let MongoClient = require('mongodb').MongoClient;
let url = "mongodb://localhost:27017/";

// Connecting to the mongodb database
MongoClient.connect(url, function(err, db)
{
    //
    if (err) throw err;

    //
    let dbo = db.db("mydb");
    let query = { Likes: "5K" };
    dbo.collection("customers").find(query).toArray(function(err, result)
    {
        //
        if (err) throw err;

        //
        console.log(result);
        db.close();
    });
});
