/* Finding All */
/*
* The find() method returns all occurences in the selection *
*/
//
let MongoClient = require('mongodb').MongoClient;
let url = "mongodb://localhost:27017/";


//
MongoClient.connect(url, function(err, db)
{
    //
    if (err) throw err;

    //
    let dbo = db.db("mydb");

    //
    dbo.collection("customers").find({}).toArray(function(err, result)
    {
        //
        if (err) throw err;

        //
        console.log(result.length);
        db.close();
    });
});
