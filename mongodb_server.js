//
let MongoClient = require('mongodb').MongoClient;
let url = "mongodb://localhost:27017/mydb";

//
MongoClient.connect(url, function(err, db)
{
    // Checking for errors
    if (err) throw err;

    // If there is a connection to the database
    console.log("Database created!");
    db.close();
})
