//
let MongoClient = require('mongodb').MongoClient;
let url = "mongodb://localhost:27017/";

//
MongoClient.connect(url, function(err, db)
{
    //
    if (err) throw err;

    //
    let dbo = db.db("mydb");
    dbo.collection("customers").find({}, { projection: { _id: 0, News: 1, UserName: 1 }
    }).toArray(function (err, result)
          {
              //
              if (err) throw err;

              //
              console.log(result);
              db.close();
          });
});
