//
let MongoClient = require('mongodb').MongoClient;
let url = "mongodb://localhost:27017/";

//
MongoClient.connect(url, function(err, db)
{
    //
    if (err) throw err;

    //
    let dbo = db.db("mydb");
    let user = { UserName: "@dectypto", Message: "#tipestry #pics #photography #nature #discussion #pics ", TimePresent: "3 days ago",
                  Likes: "10K", DisLikes: "1.9K", Comments: "2", Shares: "10", MoneyRatings: "7", ActualMoney: "1",
                  ImageURL: "https://image.thum.io/get/iphoneX/width/100/auth/3228-www.tipestry.com/https://cointelegraph.com/news/dogecoin-trading-volume-hits-5b-surpassing-bitcoin-s-for-the-first-time-ever"};
    dbo.collection("customers").insertOne(user, function(err, res)
    {
        if (err) throw err;

        //
        console.log("1 document inserted");
        db.close();
    });
});
