// Including the mongodb client
let MongoClient = require('mongodb').MongoClient;
let url = "mongodb://localhost:27017/";

// Connecting to the mongodb database
MongoClient.connect(url, function(err, db)
{
    //
    if (err) throw err;

    //
    let dbo = db.db("mydb");
    // Finding just one data from the connected mongoDB database  
    dbo.collection("customers").findOne({}, function(err, result)
    {
        //
        if (err) throw err;

        //
        console.log(result);
        db.close();
    });
});
